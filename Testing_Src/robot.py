'''
Created by Gatlen Culp 23 December 2018
Last updated 18 January 2019

This is the Master File that runs all the other files.
'''

import wpilib, magicbot
# from pyconsole_util import util
from robotmap import RobotMap as Map
from components.drive import Drive
from components.ball_arm import BallArm
from components.hatch_arm import HatchArm
from components.habitat_climber import HabitatClimber
from controller import Controller

# MagicRobot api: https://robotpy.readthedocs.io/en/latest/frameworks/magicbot.html
class Robot(magicbot.MagicRobot):

    # Modes: ball, hatch, hab
    mode = "hatch"
    drive_is_relative = False
    direction = 1 # 1 is towards hatch arm/hab, -1 is towards ball arm

    map = Map()

    def createObjects(self):
        # util.title("createObjects Running")

        self.drive = Drive(self)
        self.controller = Controller(self, self.map.joystick.XBOX_CONTROLLER_PORT)
        self.ball_arm = BallArm(self)
        self.hatch_arm = HatchArm(self)
        self.habitat_climber = HabitatClimber(self)

    def disabledPeriodic(self):
        pass
        # Ensure motors are turned off when disabled
        self.drive.controller.driveCartesian(0, 0, 0, 0)

    def teleopInit(self):
        # util.title("Teleop Initializing")
        pass

    def checkMode(self):
        original_mode = self.mode

        if self.controller.x_pressed: self.mode = "hatch"
        elif self.controller.y_pressed: self.mode = "ball"
        elif self.controller.b_pressed: self.mode = "hab"

        if self.mode != original_mode: print("Changed mode to", self.mode)

    def runMode(self):
        # If current camera is not the one for this side, switch
        if self.mode == "hatch":
            self.direction = 1

            if self.controller.a_pressed: self.hatch_arm.punch()
            else: self.hatch_arm.retract()

            self.hatch_arm.adjustArm(
                self.controller.lt - self.controller.rt
            )

        elif self.mode == "ball":
            self.direction = -1

            if self.controller.lb_pressed: self.ball_arm.rollIntake(-1)
            elif self.controller.rb_pressed: self.ball_arm.rollIntake(1)
            else: self.ball_arm.rollIntake(0)
            # Also works but is bad: self.ball_arm.rollIntake(self.controller.rb_pressed - self.controller.rb_pressed)

            self.ball_arm.adjustArm(
                self.controller.rt - self.controller.lt
            )
        elif self.mode == "hab":
            self.direction = 1

            # Fix it so that the button doesn't constantly flip
            if self.controller.a_just_pressed:
                if self.habitat_climber.is_clamped: self.habitat_climber.releaseClamp()
                else: self.habitat_climber.clamp()

            self.habitat_climber.adjustArm(
                self.controller.lt - self.controller.rt
            )

        else: print("ERROR: INVALID MODE!!!")

    def teleopPeriodic(self):

        self.controller.read(self)
        self.controller.adjustInput()

        if self.controller.start_just_pressed:
            self.drive_is_relative = False
            print("Drive mode is now bot based")
        elif self.controller.back_just_pressed:
            self.drive_is_relative = True
            print("Drive mode is now field based")

        self.drive.controlDrive(self)
        self.checkMode()
        self.runMode()

if __name__ == "__main__":
    wpilib.run(Robot, physics_enabled=True)
