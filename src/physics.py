from robotmap import RobotMap as Map
from sim.config import robot_info
from pyfrc.physics.drivetrains import MecanumDrivetrain
from pyfrc.physics.units import units

class PhysicsEngine(object):
    # Required class for physics
    first_run = True

    def __init__(self, physics_controller):
        # constructor must accept a physics_controller
        self.physics_controller = physics_controller
        self.drive = MecanumDrivetrain(
            x_wheelbase=(robot_info["width"])/12,
            y_wheelbase=(robot_info["height"])/12,
            speed=robot_info["speed"],
            deadzone=None
        )

    def initialize(self, hal_data):
        pass

    def update_sim(self, hal_data, now, tm_diff):
        # Called each time phyiscs updates
        # hal_data is a dictionary of robot data
        # now is the current time
        # tm_diff is the amount of time since this was last updated
        if self.first_run:
            self.first_run = False

        # hal_data["pwm"] contains all the information from the robot setup in robot.py
        # Saves front left and right motor info, only takes the value and applies to whole channel
        front_left_motor = hal_data["CAN"][Map.can.drive.DRIVE_FRONT_LEFT_WHEEL]["value"]
        front_right_motor = hal_data["CAN"][Map.can.drive.DRIVE_FRONT_RIGHT_WHEEL]["value"]
        back_left_motor = hal_data["CAN"][Map.can.drive.DRIVE_BACK_LEFT_WHEEL]["value"]
        back_right_motor = hal_data["CAN"][Map.can.drive.DRIVE_BACK_RIGHT_WHEEL]["value"]

        # DISCLAIMER: Doesn't drive correctly
        # Saves the displacement the robot has made and then applies that to the sim
        directionVector = self.drive.get_vector(
            lr_motor=back_left_motor, rr_motor=back_right_motor*-1, lf_motor=front_left_motor, rf_motor=front_right_motor*-1
        )
        self.physics_controller.vector_drive(*directionVector, tm_diff)
