'''
Started by Gatlen Culp on 23 December 2018

This is the entry point for the code
'''

import wpilib, magicbot, time
from networktables import NetworkTables
from robotmap import RobotMap
from components.subsystems import BallArm, Drive, HabitatClimber, HatchArm
from components.hardware import XboxController
from components.abstractions import PowerOrientedArm, AngleOrientedArm

class Robot(magicbot.MagicRobot):

    mode = "ball" # ball, hatch, hab
    targeting_mode = "none" # none, ball, tape
    direction = 1 # 1 hatch, hab, -1 ball (multiplies with ForwardDrive)
    # direction also determines the active camera

    delta_time = 0
    last_time = time.time()

    map = RobotMap()

    def createObjects(self):
        # Network tables
        NetworkTables.initialize()
        self.smart_dashboard = NetworkTables.getTable("SmartDashboard")
        self.live_window = NetworkTables.getTable("LiveWindow")
        self.main_network_table = NetworkTables.getTable("Main")
        self.vision_network_table = NetworkTables.getTable("Vision")

        # Subsytems
        self.drive = Drive(self)
        self.controller = XboxController(self, self.map.usb.XBOX_CONTROLLER_PORT)
        self.ball_arm = BallArm(self)
        self.hatch_arm = HatchArm(self)
        self.habitat_climber = HabitatClimber(self)
        self.robot_controller = wpilib.RobotController()

        # Hardware
        self.gyro = wpilib.AnalogGyro(self.map.analog.GYRO)
        self.gyro.calibrate()

        # Values
        self.delta_time = 0 # Measured in seconds since last loop

    def disabledPeriodic(self):
        self.drive.disable()
        self.ball_arm.disable()
        self.hatch_arm.disable()
        self.habitat_climber.disable()
        self.main_network_table.putNumber("Battery Voltage", self.robot_controller.getBatteryVoltage())

    def runMode(self, mode):
        self.main_network_table.putString("Mode", mode)

        if mode == "hatch":
            self.ball_arm.disable()
            self.habitat_climber.disable()
            self.direction = 1
            self.hatch_arm.operate()
        elif mode == "ball":
            self.hatch_arm.anchor()
            self.habitat_climber.disable()
            self.direction = -1
            self.ball_arm.operate()
            if self.controller.a_pressed:
                self.unfold()
                # print("After unfold", self.ball_arm.angling_motors["left_angle_motor"].get())
        elif mode == "hab":
            self.hatch_arm.anchor()
            self.direction = 1
            if self.habitat_climber.is_clamped:
                self.ball_arm.rawRotate(-0.15)
            self.habitat_climber.operate()
        else: print("ERROR: Invalid drive mode selected!")

    def unfold(self):
        pass
        # if self.habitat_climber.is_folded: self.habitat_climber.unfold()
        # else:
        #     if self.hatch_arm.is_folded: self.hatch_arm.unfold()
        #     if self.ball_arm.is_folded: self.ball_arm.unfold()
        # if self.hatch_arm.is_folded: self.hatch_arm.unfold()
    #     if self.ball_arm.is_folded: self.ball_arm.unfold()

    def update(self):
        self.drive.update()
        self.hatch_arm.update()
        self.ball_arm.update()
        self.habitat_climber.update()
        self.main_network_table.putNumber("Battery Voltage", self.robot_controller.getBatteryVoltage())

    def teleopInit(self):
        self.habitat_climber.release_servo.set(0)
        # self.ball_arm.encoder.reset()

    def teleopPeriodic(self):
        self.delta_time = (time.time() - self.last_time)

        self.controller.read()
        self.controller.checkForPowerChange()
        self.controller.checkForModeChange()
        self.controller.checkForGyroCalibration()
        self.drive.controlDrive(self)
        self.runMode(self.mode)
        # print("Hatch Arm Angle:", self.hatch_arm.getAngle(), "Ball Arm Angle:", self.ball_arm.getAngle())
        # print("Limit switch:", self.habitat_climber.limit_switch.isTriggered())
        # print("After teleop", self.ball_arm.angling_motors["left_angle_motor"].get())
        # print(self.hatch_arm.positions[self.hatch_arm.position])
        self.update()

        self.last_time = time.time()

        '''
        For unfolding, I think it might be much better if it was all done automatically.

        When moving the hatch arm up and it is behind the hab arm
            Stop the hatch arm from moving
            Move the hab arm up
            Move the hatch arm past the hab arm
            Move the hab arm back
        
        When moving the hatch arm back and it is in front of the hab arm
            Stop the hatch arm from moving
            Move the hab arm up
            Move the hatch arm past the hab arm
            Move the hab arm back

        Both of the things described above are basically the same thing just for different directions of the hatch arm

        (I NEED TO FIGURE OUT WHAT UNFOLDING MUST HAPPEN)
        '''

if __name__ == "__main__":
    wpilib.run(Robot, physics_enabled=True)
