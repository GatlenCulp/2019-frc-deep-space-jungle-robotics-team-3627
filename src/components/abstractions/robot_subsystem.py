'''
RobotComponent is a component on the robot. 

It takes in all of the parts (motors, encoders, etc.) and determines which to shut off
'''

import ctre, wpilib
from components.hardware import RobotMotor

def flatten_dict(d):
    def items():
        for key, value in d.items():
            if isinstance(value, dict):
                for subkey, subvalue in flatten_dict(value).items():
                    yield key + "." + subkey, subvalue
            else:
                yield key, value

    return dict(items())

'''
Base Class
'''

retracted = 2
extended = 1

class RobotSubsystem:
    def __init__(self, robot):
        self.parts = flatten_dict(self.__dict__)
        self.robot = robot

    def disable(self):
        for _, part in self.parts.items():

            # I believe you don't need to use RobotMotor bc RobotMotor is an instance of another motor
            if (isinstance(part, RobotMotor)
            or isinstance(part, ctre.WPI_VictorSPX)
            or isinstance(part, ctre.WPI_TalonSRX)):
                # print("Disabling", part)
                part.set(0)
            
            if (isinstance(part, wpilib.DoubleSolenoid)):
                part.set(retracted)
    
    def listParts(self):
        print(*self.parts.items(), sep="\n")