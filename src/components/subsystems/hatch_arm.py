import ctre, wpilib
from components.abstractions import AngleOrientedArm # PowerOrientedArm
from components.hardware import RobotPotentiometer

retracted = 2
extended = 1

# TODO: Don't allow the ability to turn on pneumatics when below a certain point

class HatchArm(AngleOrientedArm):

    is_folded = True

    def __init__(self, robot):
        
        self.puncher = wpilib.DoubleSolenoid(
            forwardChannel=robot.map.pcm.PUNCHER_FORWARD, reverseChannel=robot.map.pcm.PUNCHER_BACKWARD
        )

        super().__init__(
            robot,
            angling_motors = {"hatch_arm_angling_motor": ctre.WPI_VictorSPX(robot.map.can.HATCH_ARM_ANGLER)},
            positions=[200, 90, 80, -15],
            potentiometer=RobotPotentiometer(robot.map.analog.HATCH_ARM_POTENTIOMETER)
            # angle_power_up = 0.8,
            # angle_power_down = 0.5
        )

    def punch(self):
        # Don't extend, arm is too far downward
        if self.getAngle() < self.min_angle: return
        
        self.puncher.set(extended)

    def retract(self):
        self.puncher.set(retracted)

    # def unfold(self):
    #     if self.getAngle() > 80: self.rotate(-1)
    #     else: self.is_folded = False

    def operate(self):
        if self.robot.controller.a_pressed: self.punch()
        else: self.retract()

        if self.robot.controller.right_bumper_just_pressed: self.nextPosition()
        elif self.robot.controller.left_bumper_just_pressed: self.prevPosition()

        # user_input = self.robot.controller.left_trigger - self.robot.controller.right_trigger

        # if user_input == 0: self.anchor()
        # else: self.rotate(user_input)
    
    def setAngle(self, target_angle): # 90 degrees = 100% power, error/90
        Kp = 2
        error = target_angle - self.angle # 90 - 180, -90
        # print("setAngle:", target_angle, "current angle:", self.angle)
        if self.angle < 90:
            self.rawRotate((Kp * error)/90 + 0.1)
        else:
            self.rawRotate((Kp * error)/90)
        # if target_angle > self.angle: self.rawRotate((Kp * error)/90)
        # elif target_angle < self.angle: self.rotate(-0.5)
        # else: self.anchor()
        pass

    def update(self):
        # Has an encoder
        if self.encoder != None:
            self.angle = self.encoder.getAngle()
        # Has a potentiometer
        elif self.potentiometer != None:
            self.angle = self.potentiometer.getAngle()
        
        # print(self.angle)

        self.setAngle(self.positions[self.position])
