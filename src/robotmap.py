'''
This script serves to create a list of ports and channels that can be easily
changed if someone were to say change the ID of a CAN motor.
'''

class Drive:
    DRIVE_FRONT_LEFT_WHEEL = 1
    DRIVE_FRONT_RIGHT_WHEEL = 2
    DRIVE_BACK_LEFT_WHEEL = 3
    DRIVE_BACK_RIGHT_WHEEL = 4

class CAN:
    drive = Drive()

    HATCH_ARM_ANGLER = 10

    BALL_ARM_ANGLER_LEFT = 21
    BALL_ARM_ANGLER_RIGHT = 20
    BALL_ARM_INTAKE = 23

    HAB_CLIMBER_LEFT_ANGLING_MOTOR = 30
    HAB_CLIMBER_RIGHT_ANGLING_MOTOR = 31

class PWM:
    HAB_CLIMBER_RELEASE_SERVO = 0

class PCM:
    PUNCHER_FORWARD = 0
    PUNCHER_BACKWARD = 1
    HAB_CLIMBER_ACTUATE = 6
    HAB_CLIMBER_REVERSE = 7

# On laptop
class USB:
    XBOX_CONTROLLER_PORT = 0

class Analog:
    GYRO = 0
    HATCH_ARM_POTENTIOMETER = 1
    BALL_ARM_POTENTIOMETER = 2

class DIO:
    HAB_CLIMBER_LIMIT_SWITCH = 0

class RobotMap:
    can = CAN()
    pwm = PWM()
    pcm = PCM()
    dio = DIO()
    usb = USB()
    analog = Analog()

# Used for testing if the RobotMap works
def test():
    pass

def bruh():
    print("bruh")

# Doesn't run in final code
if __name__ == "__main__":
    test()
    bruh()